
	/**
	  *   Aufgabe:  Recherechieren Sie im Internet !
	  * 
	  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
	  *
	  *   Vergessen Sie nicht den richtigen Datentyp !!
	  *
	  *
	  * @version 1.0 from 21.08.2019
	  * @author << Ihr Name >>
	  */

	public class WeltderZahlen {

	  public static void main(String[] args) {
	    
	    /*  *********************************************************
	    
	         Zuerst werden die Variablen mit den Werten festgelegt!
	    
	    *********************************************************** */
	    // Im Internet gefunden ?
	    // Die Anzahl der Planeten in unserem Sonnesystem                    
	    int anzahlPlaneten = 8  ;
	    
	    // Anzahl der Sterne in unserer Milchstra�e
	    short anzahlSterne = 600;  //in Milliarden
	    
	    // Wie viele Einwohner hat Berlin?
	    int bewohnerBerlin = 3700000;
	    
	    // Wie alt bist du?  Wie viele Tage sind das?
	    
	    short alterTage = 25*365;
	    
	    // Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
	    short gewichtKilogramm =  18000;
	    
	    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
	    int flaecheGroessteLand = 17098242; 
	    
	    // Wie gro� ist das kleinste Land der Erde?
	    
	    double flaecheKleinsteLand = 123.28;
	    
	    
	    
	    
	    /*  *********************************************************
	    
	         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
	    
	    *********************************************************** */
	    System.out.println(" *******  Anfang des Programms  ******* ");
	    System.out.println("Anzahl der Planeten: 			" + anzahlPlaneten);
	    
	    System.out.println("Anzahl der Sterne in Milliarden:	" + anzahlSterne );
	    
	    System.out.println("Anzahl Einwohner Berlin:		" + bewohnerBerlin);
	    System.out.println("Mein Alter in Tagen:			" + alterTage);
	    System.out.println("Fl�che des gr��ten Land in km^2:	" + flaecheGroessteLand);
	    System.out.println("Fl�che des kleinsten Land in km^2: 	" + flaecheKleinsteLand);
	    
	    
	    
	    
	    System.out.println(" *******  Ende des Programms  ******* ");
	    
	  }
}


