

//@ Nils Derwell
//@ Version 1

public class VariablenVerwenden {
public static void main(String [] args){
/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
      Vereinbaren Sie eine geeignete Variable */
	
	int durchlaeufe;
	
/* 2. Weisen Sie dem Zaehler den Wert 25 zu
      und geben Sie ihn auf dem Bildschirm aus.*/

	durchlaeufe = 25;
	System.out.println("Programmdruchlaeufe: " + durchlaeufe);
	
/* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
      eines Programms ausgewaehlt werden.
      Vereinbaren Sie eine geeignete Variable */
	
	char menuepunkt ;

/* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
      und geben Sie ihn auf dem Bildschirm aus.*/
	
	menuepunkt = 'C';
	System.out.println("Menuepunkt: " + menuepunkt);

/* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
      notwendig.
      Vereinbaren Sie eine geeignete Variable */
	
	long astronomischeZahlen ;

/* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
      und geben Sie sie auf dem Bildschirm aus.*/
	
	astronomischeZahlen = 299792458;
	System.out.println("Lichtgeschwindigkeit: " + astronomischeZahlen + " m/s");

/* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
      soll die Anzahl der Mitglieder erfasst werden.
      Vereinbaren Sie eine geeignete Variable und initialisieren sie
      diese sinnvoll.*/
	
	short anzahlMitglieder ;
	anzahlMitglieder = 7;

/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	
	System.out.println("Anzahl derMitglieder: " + anzahlMitglieder);

/* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
      Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
      dem Bildschirm aus.*/
	
	String elektrischeLadung = "e^-";
	System.out.println("Elektronladung: " + elektrischeLadung);

/*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
      Vereinbaren Sie eine geeignete Variable. */
	
	boolean zahlung ;

/*11. Die Zahlung ist erfolgt.
      Weisen Sie der Variable den entsprechenden Wert zu
      und geben Sie die Variable auf dem Bildschirm aus.*/

	zahlung = true;
	System.out.println("Zahlungseingang: " + zahlung);
	
}//main
}// Variablen